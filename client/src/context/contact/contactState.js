import React, { useReducer } from 'react';
import axios                 from 'axios';
import ContactContext        from './contactContext';
import contactReducer        from './contactReducer';
import {
	GET_CONTACTS,
	ADD_CONTACT,
	DELETE_CONTACT,
	SET_CURRENT,
	CLEAR_CURRENT,
	UPDATE_CONTACT,
	FILTER_CONTACTS,
	CLEAR_CONTACTS,
	CLEAR_FILTER,
	CONTACT_ERROR
}                            from '../types';
import { CONTACT_ENDPOINT }  from '../../config'

const ContactState = props => {
	const initialState = {
		contacts: null,
		current : null,
		filtered: null,
		error   : null
	};

	const [ state, dispatch ] = useReducer(contactReducer, initialState);

	// Get Contacts
	const getContacts = async () => {
		try {
			const res = await axios.get(CONTACT_ENDPOINT);

			dispatch({
				type   : GET_CONTACTS,
				payload: res.data
			});
		} catch (err) {
			dispatch({
				type   : CONTACT_ERROR,
				payload: err.response.msg
			});
		}
	};

	// Add Contact
	const addContact = async contact => {
		try {
			const res = await axios.post(CONTACT_ENDPOINT, contact);

			dispatch({
				type   : ADD_CONTACT,
				payload: res.data
			});
		} catch (err) {
			dispatch({
				type   : CONTACT_ERROR,
				payload: err.response.data.errors ? err.response.data.errors.map(item => item.msg) : err.response.data
			});
		}
	};

	// Delete Contact
	const deleteContact = async id => {
		try {
			await axios.delete(`${CONTACT_ENDPOINT}/${id}`);

			dispatch({
				type   : DELETE_CONTACT,
				payload: id
			});
		} catch (err) {
			dispatch({
				type   : CONTACT_ERROR,
				payload: err.response.msg
			});
		}
	};

	// Update Contact
	const updateContact = async contact => {
		try {
			const res = await axios.put(`${CONTACT_ENDPOINT}/${contact._id}`, contact);

			dispatch({
				type   : UPDATE_CONTACT,
				payload: res.data
			});
		} catch (err) {
			dispatch({
				type   : CONTACT_ERROR,
				payload: err.response.msg
			});
		}
	};

	// Clear Contacts
	const clearContacts = () => {
		dispatch({ type: CLEAR_CONTACTS });
	};

	// Set Current Contact
	const setCurrent = contact => {
		dispatch({ type: SET_CURRENT, payload: contact });
	};

	// Clear Current Contact
	const clearCurrent = () => {
		dispatch({ type: CLEAR_CURRENT });
	};

	// Filter Contacts
	const filterContacts = text => {
		dispatch({ type: FILTER_CONTACTS, payload: text });
	};

	// Clear Filter
	const clearFilter = () => {
		dispatch({ type: CLEAR_FILTER });
	};

	return (
		<ContactContext.Provider
			value={{
				contacts: state.contacts,
				current : state.current,
				filtered: state.filtered,
				error   : state.error,
				addContact,
				deleteContact,
				setCurrent,
				clearCurrent,
				updateContact,
				filterContacts,
				clearFilter,
				getContacts,
				clearContacts
			}}
		>
			{props.children}
		</ContactContext.Provider>
	);
};

export default ContactState;
