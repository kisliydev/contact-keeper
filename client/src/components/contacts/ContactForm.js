import React, { useState, useContext, useEffect } from 'react';

import ContactContext from '../../context/contact/contactContext'
import AlertContext   from '../../context/alert/alertContext';
import { isEmpty }    from '../../utils/helper';

const ContactForm = () => {

	const contactContext = useContext(ContactContext);
	const alertContext = useContext(AlertContext);

	const { addContact, updateContact, clearCurrent, current, error } = contactContext;
	const { setAlert } = alertContext;

	useEffect(() => {
		if (!isEmpty(error)) {
			setAlert(error, 'danger');
		}

		if (current !== null) {
			return setContact(current);
		}

		return setContact({
			name : '',
			email: '',
			phone: '',
			type : 'personal'
		});

		// eslint-disable-next-line
	}, [ current, error, clearCurrent ]);

	const [ contact, setContact ] = useState({
		name : '',
		email: '',
		phone: '',
		type : 'personal'
	});

	const { name, email, phone, type } = contact;

	const onChange = e => setContact({ ...contact, [ e.target.name ]: e.target.value });

	const onSubmit = e => {
		e.preventDefault();

		(current === null) ? addContact(contact) : updateContact(contact);

		clearAll();
	};

	const clearAll = () => clearCurrent();

	return (
		<form onSubmit={onSubmit}>
			<h2 className='text-primary'>{current ? 'Edit Contact' : 'Add Contact'}</h2>
			<input
				type='text'
				placeholder='Name'
				name='name'
				value={name}
				onChange={onChange}/>
			<input
				type='email'
				placeholder='E-mail'
				name='email'
				value={email}
				onChange={onChange}
			/>
			<input
				type='text'
				placeholder='Phone'
				name='phone'
				value={phone}
				onChange={onChange}
			/>
			<h5>Contact type</h5>
			<input
				type='radio'
				id='personalType'
				name='type'
				value='personal'
				checked={type === 'personal'}
				className='mr'
				onChange={onChange}
			/>
			<label htmlFor='personalType' className='mr-2'>Personal</label>
			<input
				type='radio'
				id='professionalType'
				name='type'
				value='professional'
				checked={type === 'professional'}
				className='mr'
				onChange={onChange}
			/>
			<label htmlFor='professionalType'>Professional</label>
			<div>
				<input
					type='submit'
					value={current ? 'Update Contact' : 'Add Contact'}
					className='btn btn-primary btn-block'
				/>
			</div>
			{current && <button className='btn btn-light btn-block' onClick={clearAll}> Clear</button>}
		</form>
	);
};

export default ContactForm;
